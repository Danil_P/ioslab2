//
//  ATableViewController.swift
//  lab23pd
//
//  Created by Илья Осипов on 09.10.17.
//  Copyright © 2017 KBTU. All rights reserved.
//

import UIKit

class ATableViewController: UITableViewController {

    var delegate: GoBackDelegate?
    
    var totalScore:Int = 0;
    var answers:Array<Answer> = [];
    
    @IBOutlet weak var total: UILabel!
    @IBAction func goHome(_ sender: Any) {
        delegate?.goBack();
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        total.text = "Total: " + String(totalScore) + "/" + String(answers.count);

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cell.question.text = answers[indexPath.row].question
        cell.currentAns.text = answers[indexPath.row].currentAnswer
        cell.correctAns.text = answers[indexPath.row].rightAnswer
        cell.Color(tORf: answers[indexPath.row].isCorrect())
        return cell
    }
    
    func inite(arrayOfAns: Array<Answer>){
        answers = arrayOfAns;
        
        for answer in answers {
            if answer.isCorrect() { totalScore += 1; }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count;
    }

}
