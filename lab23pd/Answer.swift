//
//  Answer.swift
//  lab23pd
//
//  Created by Илья Осипов on 09.10.17.
//  Copyright © 2017 KBTU. All rights reserved.
//

import Foundation

class Answer{
    var question:String?;
    var currentAnswer:String?;
    var rightAnswer:String?;
    
    init(question: String, currentAnswer:String, rightAnswer:String){
        self.question = question;
        self.currentAnswer = currentAnswer;
        self.rightAnswer = rightAnswer;
    }
    
    func isCorrect() -> Bool {
        return currentAnswer == rightAnswer;
    }
    
}
