//
//  SecondViewController.swift
//  lab23pd
//
//  Created by user on 07.10.17.
//  Copyright © 2017 KBTU. All rights reserved.
//

import UIKit

protocol GoBackDelegate {
    func goBack()
}
extension Dictionary {
    subscript(i:Int) -> (key:Key,value:Value) {
        get {
            return self[index(startIndex, offsetBy: i)];
        }
    }
}
class SecondViewController: UIViewController, GoBackDelegate {
    
    @IBOutlet weak var labelQ: UILabel!
    
    var listAnswer: Array<Answer> = []
    
    var array = [["Kingsman", "Karate Kid", "U.N.C.L.E"],
                 ["Stan Smith", "Homer Simpson", "Peter Griffin"],
                 ["Yandex", "Kinopoisk", "Mail.ru"]]
    
    var dict: Dictionary = ["1 Jackice Chan Movie":"Karate Kid",
                            "2 South Park character":"Stan Smith",
                            "3 Search site":"Yandex"]
    var cur: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generateQuestion()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
        reset()
        generateQuestion()
    }
    
    func generateQuestion() {
        labelQ.text = dict.sorted(by: <)[cur].key
        let variant = array[cur]
        for i in 0..<variant.count{
            let button = UIButton(frame: CGRect(x: 0, y: 42*i+150, width: Int(UIScreen.main.bounds.size.width), height: 40))
            button.backgroundColor = UIColor.gray
            button.setTitle(variant[i], for: UIControlState.normal)
            button.addTarget(self, action:#selector(buttonClicked(_:)), for: .touchUpInside)
            self.view.addSubview(button)
        }
    }
    
    func buttonClicked(_ sender:UIButton) {
        let answer: Answer = Answer(question: labelQ.text!, currentAnswer: sender.currentTitle!, rightAnswer: dict.sorted(by: <)[cur].value)
        listAnswer.append(answer)
        
        if cur < dict.count - 1 {
            cur += 1
            generateQuestion()
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "answers") as! ATableViewController
            vc.delegate  = self
            vc.inite(arrayOfAns: listAnswer)
            reset()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func reset() {
        cur = 0
        listAnswer.removeAll();
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
